
import com.eot.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class GameTest {


    @Test
    public void shouldUpdatePlayersScoreForCheatRound() {

        IOStrategy mockScanner = mock(IOStrategy.class);
        Game game = createGame(mockScanner, 1);


        when(mockScanner.readInput()).thenReturn("ch").thenReturn("ch");

        game.play();

        assertEquals(0, game.getPlayerOneScore());
        assertEquals(0, game.getPlayerTwoScore());

    }

    private Game createGame(IOStrategy mockScanner, int round) {
        Player player = new Player(mockScanner);
        Player player1 = new Player(mockScanner);
        return new Game(player, player1, round);
    }


    @Test
    public void shouldUpdatePlayersScore() {

        IOStrategy mockScanner = mock(IOStrategy.class);
        Game game = createGame(mockScanner, 1);
        when(mockScanner.readInput()).thenReturn("co").thenReturn("co").thenReturn("ch").thenReturn("co");

        game.play();
        game.play();

        assertEquals(5, game.getPlayerOneScore());
        assertEquals(1, game.getPlayerTwoScore());

    }

    @Test
    public void shouldUpdatePlayersScoreForCooperateRound() {

        IOStrategy mockScanner = mock(IOStrategy.class);
        Game game = createGame(mockScanner, 1);
        when(mockScanner.readInput()).thenReturn("co").thenReturn("co");

        game.play();

        assertEquals(2, game.getPlayerOneScore());
        assertEquals(2, game.getPlayerTwoScore());

    }

    @Test
    public void shouldUpdatePlayersScoreForMultipleRound() {

        IOStrategy mockScanner = mock(IOStrategy.class);
        Game game = createGame(mockScanner, 2);
        when(mockScanner.readInput()).thenReturn("co").thenReturn("co").thenReturn("ch").thenReturn("co");

        game.play();

        assertEquals(5, game.getPlayerOneScore());
        assertEquals(1, game.getPlayerTwoScore());

    }

    @Test
    public void shouldUpdatePlayersScoreForAlwaysCheatAlwaysCooperate() {

        Game game = new Game(new Player(new AlwaysCheatStrategy()), new Player(new AlwaysCooperateIOStrategy()), 1);
        game.play();

        assertEquals(3, game.getPlayerOneScore());
        assertEquals(-1, game.getPlayerTwoScore());

    }

    @Test
    public void shouldUpdatePlayersScoreForAlwaysCheatAlwaysCooperateForMultipleRounds() {

        Game game = new Game(new Player(new AlwaysCheatStrategy()), new Player(new AlwaysCooperateIOStrategy()), 5);
        game.play();

        assertEquals(15, game.getPlayerOneScore());
        assertEquals(-5, game.getPlayerTwoScore());

    }


    @Test
    public void shouldAskForChoice() {
        Console mockConsole = mock(Console.class);
        Game game = new Game(new Player(new AlwaysCheatStrategy()), new Player(new AlwaysCooperateIOStrategy()), mockConsole, 1);
        game.play();
        verify(mockConsole).writeOutput("Player1 Enter UR Choice");
    }

    @Test
    public void shouldPlayFiveRoundsForCopyCatAndAlwaysCheat() {
        Console mockConsole = mock(Console.class);
        Game game = new Game(new Player(new AlwaysCheatStrategy()),new Player(new CopyCatIOStrategy()),mockConsole,5);
        game.play();

        assertEquals(3,game.getPlayerOneScore());
        assertEquals(-1,game.getPlayerTwoScore());
    }
}
