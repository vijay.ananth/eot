import com.eot.Machine;
import com.eot.ScoreCard;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MachineTest {


    @Test
    public void shouldReturn0ForBothCheats(){
        ScoreCard result = Machine.playRound("ch","ch");
        assertEquals(0, result.player1Score);
        assertEquals(0, result.player2Score);
    }

    @Test
    public void shouldReturn2ForBothCooperates(){
        ScoreCard result = Machine.playRound("co","co");
        assertEquals(2, result.player1Score);
        assertEquals(2, result.player2Score);
    }

    @Test
    public void shouldReturnScoreForCooperateCheat(){
        ScoreCard result = Machine.playRound("co","ch");
        assertEquals(-1, result.player1Score);
        assertEquals(3, result.player2Score);
    }

    @Test
    public void shouldReturnScoreForCheatCooperate(){
        ScoreCard result = Machine.playRound("ch","co");
        assertEquals(3, result.player1Score);
        assertEquals(-1, result.player2Score);

    }
}
