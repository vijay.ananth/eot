import com.eot.*;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class PlayerTest {

    @Test
    public void shouldUpdateScore()
    {
        Player player = new Player(mock(IOStrategy.class));
        player.updateScore(3);

        assertEquals(3,player.getScore());
    }

    @Test
    public void shouldUpdateScoreIncremental()
    {
        Player player = new Player(mock(IOStrategy.class));
        player.updateScore(3);
        player.updateScore(1);
        assertEquals(4,player.getScore());
    }

    @Test
    public void shopuldReturnPlayerChoice(){
        IOStrategy input = mock(IOStrategy.class);
        Mockito.when(input.readInput()).thenReturn("ch");

        Player player = new Player(input);

        assertEquals("ch",player.getChoice());
    }

    @Test
    public void shouldAlwaysReturnCheatForCheatBot()
    {
        Player cheatplayer = new Player(new AlwaysCheatStrategy());
        assertEquals("ch",cheatplayer.getChoice());
    }


    @Test
    public void shouldAlwaysReturnCooperateForCooperateBot() {
        Player cooperatePlayer =new Player(new AlwaysCooperateIOStrategy());

        assertEquals("co", cooperatePlayer.getChoice());
    }

    @Test
    public void shouldReturnCopyOfOpponentsPreviousChoice(){
        Player copyCatPlayer = new Player(new CopyCatIOStrategy());
        assertEquals("co",copyCatPlayer.getChoice());

        copyCatPlayer.updateScore(0);
        assertEquals("ch",copyCatPlayer.getChoice());
    }
}
