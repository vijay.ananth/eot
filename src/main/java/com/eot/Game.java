package com.eot;

public class Game {

    private Player player1;
    private Player player2;
    private int rounds;
    private Console console;



    public Game(Player player, Player player1, int rounds) {
        this.player1 = player;
        this.player2 = player1;
        this.rounds = rounds;
        this.console = new Console();
    }

    public Game(Player player, Player player1,Console console, int rounds) {
        this(player,player1,rounds);
        this.console = console;
    }


    public void play() {
        for (int i = 0; i < rounds; i++) {
            console.writeOutput("Player1 Enter UR Choice");
            String player1Choice = player1.getChoice();
            String player2Choice = player2.getChoice();
            ScoreCard result = Machine.playRound(player1Choice,player2Choice);
            player1.updateScore(result.player1Score);
            player2.updateScore(result.player2Score);
        }

    }

    public int getPlayerOneScore() {
        return player1.getScore();
    }

    public int getPlayerTwoScore() {
        return player2.getScore();
    }

}
