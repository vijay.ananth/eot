package com.eot;

import java.util.HashMap;
import java.util.Map;

public class Machine {

    private static Map<String, ScoreCard > scoreTable = new HashMap<>();

    static {
        scoreTable.put("ch-ch", new ScoreCard(0,0));
        scoreTable.put("ch-co", new ScoreCard(3,-1));
        scoreTable.put("co-ch", new ScoreCard(-1,3));
        scoreTable.put("co-co", new ScoreCard(2,2));

    }

    public static ScoreCard playRound(String p1Input, String p2Input) {
        return scoreTable.get(p1Input+"-"+p2Input);
    }
}
