package com.eot;

import java.util.Scanner;

public class Console implements IOStrategy {
    private Scanner scanner = new Scanner(System.in);
    @Override
    public String readInput() {
        return scanner.nextLine();
    }

    @Override
    public void writeOutput(String output) {
        System.out.println(output);
    }
}
