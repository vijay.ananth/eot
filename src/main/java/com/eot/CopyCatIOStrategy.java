package com.eot;

public class CopyCatIOStrategy implements IOStrategy {
    private String choice;

    public CopyCatIOStrategy()
    {
        this.choice="co";
    }

    @Override
    public String readInput() {
        return choice;
    }

    @Override
    public void writeOutput(String output) {
        this.choice=output;
    }
}
