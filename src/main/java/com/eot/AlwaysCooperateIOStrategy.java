package com.eot;

public class AlwaysCooperateIOStrategy implements IOStrategy {
    @Override
    public String readInput() {
        return "co";
    }

    @Override
    public void writeOutput(String output) {

    }
}
