package com.eot;

public interface IOStrategy {

    String readInput();

    void writeOutput(String output);
}
