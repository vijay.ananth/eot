package com.eot;

public class AlwaysCheatStrategy implements IOStrategy {
    @Override
    public String readInput() {
        return "ch";
    }

    @Override
    public void writeOutput(String output) {

    }
}
