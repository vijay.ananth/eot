package com.eot;

public class Player {

    private int score;
    private IOStrategy input;

    public Player(IOStrategy input) {
        this.score = 0;
        this.input = input;
    }

    public void updateScore(int currentScore) {
        score += currentScore;
        if (currentScore >= 2) {
            input.writeOutput("co");
        } else {
            input.writeOutput("ch");
        }
    }

    public int getScore() {
        return score;
    }

    public String getChoice() {
        return input.readInput();
    }
}
