package com.eot;

public class Main {

    public static void main(String ...args){

        Game game = createGame(new Console(),1);

        game.play();
        System.out.println(game.getPlayerOneScore());
        System.out.println(game.getPlayerTwoScore());
    }
    private static Game createGame(IOStrategy mockScanner, int round) {
        Player player = new Player(mockScanner);
        Player player1 = new Player(mockScanner);
        return new Game(player, player1,round);
    }
}
