package com.eot;

public class ScoreCard {

    public int player1Score;
    public int player2Score;

    public ScoreCard(int p1Score, int p2Score){

        player1Score = p1Score;
        player2Score = p2Score;
    }
}
